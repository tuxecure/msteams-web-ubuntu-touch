import QtQuick 2.12
import QtQuick.Window 2.3
import Lomiri.Components 1.3 as UITK
import Lomiri.Content 1.3 as UCT

Rectangle {
    id: picker
    color: "#fff"

    property var activeTransfer

    property string filePath
    property var handler
    property var contentType
    property var selectionType
    property string headerText

    signal imported(var items)

    UCT.ContentPeerPicker {
        anchors {
            fill: parent
        }
        visible: parent.visible
        headerText: picker.headerText
        contentType: picker.contentType
        handler: picker.handler

        onPeerSelected: {
            if(picker.selectionType) {
                peer.selectionType = picker.selectionType;
            }

            picker.activeTransfer = peer.request();
            picker.activeTransfer.stateChanged.connect(() => {
                if((handler === UCT.ContentHandler.Share || handler === UCT.ContentHandler.Destination) && picker.filePath) {
                    if(picker.activeTransfer && picker.activeTransfer.state === UCT.ContentTransfer.InProgress) {
                        console.log(`Sharing ${picker.filePath}`);
                        picker.activeTransfer.items = [ resultComponent.createObject(parent, { "url": filePath }) ];
                        picker.activeTransfer.state = UCT.ContentTransfer.Charged;
                        picker.imported(null);
                    }
                }

                if(handler === UCT.ContentHandler.Source) {
                    if(picker.activeTransfer && picker.activeTransfer.state === UCT.ContentTransfer.Charged) {
                        if(picker.activeTransfer.items.length) {
                            picker.imported(Object.values(picker.activeTransfer.items).map(({ url }) => url));
                        }
                        picker.activeTransfer = null;
                    }
                }
            });
        }

        onCancelPressed: {
            picker.imported(null)
        }
    }

    UCT.ContentTransferHint {
        id: transferHint
        anchors.fill: parent
        activeTransfer: picker.activeTransfer
    }

    Component {
        id: resultComponent
        UCT.ContentItem {}
    }
}
